function search(ele) {
  if (event.key === "Enter") {
    document.getElementById(`result-top`).innerHTML = numArr;
  }
}

function inBangSo() {
  var result = "";
  for (var i = 1; i <= 10; i++) {
    for (j = i; j <= 100; j += 10) {
      if (j > 90) {
        result += j + "<br>";
      } else if (10 <= j && j <= 90) {
        result += j + "&nbsp" + " ";
      } else if (j < 10) {
        result += j + "&nbsp&nbsp&nbsp" + " ";
      }
    }
  }

  document.getElementById(`result-ex1`).innerHTML = `${result}`;

  $("#form-1").submit(function (e) {
    e.preventDefault();
  });
}

let numArr = [];

function getNumber() {
  let number = document.getElementById(`nhapmangsonguyen`).value * 1;

  document.getElementById(`nhapmangsonguyen`).value = "";

  if (Number.isInteger(number)) {
    numArr.push(number);
  }

  document.getElementById(`result-ex2_get-array`).innerHTML = numArr;

  $("#form-2").submit(function (e) {
    e.preventDefault();
  });
}

function inSoNt(Arr) {
  Arr = [];

  let primeNum = numArr.filter((num) => {
    for (let i = 2; i * i <= num; i++) if (num % i === 0) return false;
    return num > 1;
  });

  Arr.push(primeNum);

  document.getElementById(`result-ex2`).innerHTML = Arr;

  $("#form-2").submit(function (e) {
    e.preventDefault();
  });
}

function tinhS(n) {
  n = document.getElementById(`nhapgiatrin`).value * 1;
  let sum = 0;
  let result = 0;

  for (let i = 0; i <= n; i++) {
    sum += i;
    result = sum + 2 * n;
  }

  document.getElementById(
    `result-ex3`
  ).innerHTML = `Kết quả của phép tính: ${result}`;

  $("#form-3").submit(function (e) {
    e.preventDefault();
  });
}

function tinhUocSo(n) {
  var uocSoArr = [];
  n = document.getElementById(`sonex4`).value * 1;
  var result = "";

  for (var i = 0; i <= n; i++) {
    if (n % i === 0) {
      uocSoArr.push(i);
    }
  }

  document.getElementById(
    `result-ex4`
  ).innerHTML = `Các ước số của ${n}: ${uocSoArr}`;

  $("#form-4").submit(function (e) {
    e.preventDefault();
  });
}

function daoNguocSo() {
  let soN = document.getElementById(`sonex5`).value;
  let result = 0;

  result = String(soN).split("").reverse().join("");

  document.getElementById(
    `result-ex5`
  ).innerHTML = `Số đã đảo ngược là: ${result}`;

  $("#form-5").submit(function (e) {
    e.preventDefault();
  });
}

function timSoNdLonNhat() {
  let sum = 0;

  for (let i = 1; sum < 100; i++) {
    sum += i;
    if (sum > 100) break;

    document.getElementById(`result-ex6`).innerText = i;
  }

  $("#form-6").submit(function (e) {
    e.preventDefault();
  });
}

function inBcc(n) {
  n = document.getElementById(`sonex7`).value * 1;
  var result = "";
  for (var i = 1; i <= 10; i++) {
    result += `<br> ${n} x ${i} = ${n * i}`;
  }

  document.getElementById(
    `result-ex7`
  ).innerHTML = `Kết quả của phép tính: ${result}`;

  $("#form-7").submit(function (e) {
    e.preventDefault();
  });
}

function chiaBai() {
  let players = [[], [], [], []];

  var player1 = players[0];
  var player2 = players[1];
  var player3 = players[2];
  var player4 = players[3];

  let cards = [
    "4K",
    "KH",
    "5C",
    "KA",
    "QH",
    "KD",
    "2H",
    "10S",
    "AS",
    "7H",
    "9K",
    "10D",
  ];

  for (var i = 1; i <= cards.length; i++) {
    var chosenCard = cards[i];
    var cardForPlayer1 = cards.splice(chosenCard, 1);
    player1.push(cardForPlayer1);

    var cardForPlayer2 = cards.splice(chosenCard, 1);
    player2.push(cardForPlayer2);

    var cardForPlayer3 = cards.splice(chosenCard, 1);
    player3.push(cardForPlayer3);

    var cardForPlayer4 = cards.splice(chosenCard, 1);
    player4.push(cardForPlayer4);
  }

  document.getElementById(
    `result-ex8`
  ).innerHTML = `Kết quả chia bài là:<br> Player 1= ${player1} <br> Player 2= ${player2} <br> Player 3= ${player3} <br> Player 4= ${player4}`;

  $("#form-8").submit(function (e) {
    e.preventDefault();
  });
}

function demSoGaCho() {
  let n = document.getElementById(`sochan`).value * 1;
  let m = document.getElementById(`socon`).value * 1;

  let soCho = n / 2 - m;
  let soGa = 2 * m - n / 2;

  document.getElementById(
    `result-ex9`
  ).innerHTML = `Số gà là: ${soGa} <br> Số chó là: ${soCho}`;

  $("#form-9").submit(function (e) {
    e.preventDefault();
  });
}

function tinhGocLech() {
  let soGio = document.getElementById(`sogio`).value * 1;
  let soPhut = document.getElementById(`sophut`).value * 1;

  let soDoPhut = soPhut * (360 / 60);
  let doLechGio = 30 / 60;
  let soDoGio = doLechGio * (soGio * 60 + soPhut);

  let result = Math.abs(soDoPhut - soDoGio);

  document.getElementById(
    `result-ex10`
  ).innerHTML = `Thời gian: ${soGio} giờ ${soPhut} phút. <br> Kim giờ và kim phút lệch nhau ${result} độ`;

  $("#form-10").submit(function (e) {
    e.preventDefault();
  });
}
